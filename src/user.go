package main

//用户信息
type user struct {
	ID       int    `json:"id"`       //id
	Name     string `json:"name"`     //名字
	Password string `json:"password"` //密码
	Status   bool   `json:"status"`   //状态，0:有效帐户 1:无效帐户
}

type cmd struct {
	Select string `json:"select"`
}

const (
	updatePassword       = 1
	updateStatus         = 2
	updatePasswordStatus = 3
)

//定义用户组
type users []user
