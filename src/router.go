package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

//Route 结构体
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

//Routes 路由组
type Routes []Route

//NewRouter 创建路由
func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

var routes = Routes{
	Route{
		"delete",
		"DELETE",
		"/delete",
		DeleteTheUser,
	},
	Route{
		"find",
		"GET",
		"/find",
		FindTheUser,
	},
	Route{
		"add",
		"POST",
		"/add",
		AddTheUser,
	},
	Route{
		"modify",
		"PUT",
		"/modify",
		ModifyTheUser,
	},
}

//DeleteTheUser 删除用户
func DeleteTheUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("DeleteTheUser")
	var data user
	var result string
	//限制读取的缓存大小
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		fmt.Println(err)
		return
	}
	if err := r.Body.Close(); err != nil {
		fmt.Println(err)
		return
	}
	//获取user结构体
	if err := json.Unmarshal(body, &data); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			fmt.Println(err)
			return
		}
		return
	}
	//删除数据
	fmt.Println(data)
	if res := DeleteUser(data); res == true {
		result = "Delete data success!"
	} else {
		result = "Delete data fail!"
	}
	//返回删除结果
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		fmt.Println(err)
		return
	}
}

//FindTheUser 查找用户
func FindTheUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("FindTheUser")
	var dataTmp user
	var result string
	//限制读取的缓存大小
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		fmt.Println(err)
		return
	}
	if err := r.Body.Close(); err != nil {
		fmt.Println(err)
		return
	}
	//获取user结构体
	if err := json.Unmarshal(body, &dataTmp); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			fmt.Println(err)
			return
		}
		return
	}
	//查找数据
	if dataTmp.Name[0] == '*' {
		var datas users
		res := SelectAllUser(&datas)
		if res == true {
			result = fmt.Sprintf("%v", datas)
		} else {
			result = "Find users fail!"
		}
	} else {
		var data user
		res := SelectUserByName(dataTmp.Name, &data)
		if res == true {
			result = fmt.Sprintf("%v", data)
		} else {
			result = "Find user fail!"
		}
	}
	//返回查找结果
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		fmt.Println(err)
		return
	}
}

//AddTheUser 增加用户
func AddTheUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("AddTheUser")
	var data user
	var result string
	//限制读取的缓存大小
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		fmt.Println(err)
		return
	}
	if err := r.Body.Close(); err != nil {
		fmt.Println(err)
		return
	}
	//获取user结构体
	if err := json.Unmarshal(body, &data); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			fmt.Println(err)
			return
		}
		return
	}
	fmt.Println(data)
	//插入数据
	if res := InsertUser(data); res == true {
		result = "Insert data success!"
	} else {
		result = "Insert data fail!"
	}
	//返回插入结果
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		fmt.Println(err)
		return
	}
}

//ModifyTheUser 修改用户
func ModifyTheUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("ModifyTheUser")
	var data user
	var result string
	var tag = 0 //比较部分或者全部匹配 0代表无匹配 1代表只有password匹配 2代表只有status匹配 3代表全匹配
	//限制读取的缓存大小
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		fmt.Println(err)
		return
	}
	if err := r.Body.Close(); err != nil {
		fmt.Println(err)
		return
	}
	//获取user结构体
	if err := json.Unmarshal(body, &data); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println(string(body))
		return
	}
	//判断配置
	str := string(body)
	if strings.Contains(str, "name") == true {
		if strings.Contains(str, "password") == true {
			tag = updatePassword
		}
		if strings.Contains(str, "status") == true {
			tag = updateStatus
		}
		if strings.Contains(str, "password") == true && strings.Contains(str, "status") == true {
			tag = updatePasswordStatus
		}
		//更新数据
		if res := UpdateUser(data, tag); res == true {
			result = "Update data success!"
		} else {
			result = "Update data fail!"
		}
	} else {
		result = "the name was fail!"
	}

	//返回插入结果
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		fmt.Println(err)
		return
	}
}
