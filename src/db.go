package main

import (
	"database/sql"
	"fmt"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

//数据库配置
const (
	userName = "root"
	password = "klook"
	ip       = "127.0.0.1"
	port     = "3306"
	dbName   = "klook"
)

//Db数据库连接池
var db *sql.DB

func createTable(db *sql.DB, table string) bool {
	sql := `CREATE TABLE IF NOT EXISTS ` + table + `(
			id INT NOT NULL AUTO_INCREMENT,
			name VARCHAR(45) NOT NULL,
			password VARCHAR(45) NOT NULL,
			status INT NOT NULL DEFAULT 0,
			PRIMARY KEY (id),
			UNIQUE KEY idx_user_01 (name ASC)
			)ENGINE=InnoDB
			DEFAULT CHARACTER SET = utf8;`
	fmt.Println("\n" + sql + "\n")
	smt, err := db.Prepare(sql)
	if err != nil {
		fmt.Println(err)
		return false
	}
	smt.Exec()
	return true
}

func init() {
	//构建信息："用户名:密码@tcp(IP:端口)/数据库?charset=utf8"
	path := strings.Join([]string{userName, ":", password, "@tcp(", ip, ":", port, ")/", dbName, "?charset=utf8"}, "")

	dbtmp, err := sql.Open("mysql", path)
	if err != nil {
		fmt.Println(err)
		return
	}
	db = dbtmp
	//设置数据库最大连接数
	db.SetConnMaxLifetime(100)
	//设置上数据库最大闲置连接数
	db.SetMaxIdleConns(10)
	//验证连接
	if err := db.Ping(); err != nil {
		fmt.Println(err)
		return
	}
	//创建数据库
	if createTable(db, "user") == false {
		return
	}
	fmt.Println("connnect success")

}

//SelectUserByName 单行查询
func SelectUserByName(name string, data *user) bool {
	err := db.QueryRow("SELECT * FROM user WHERE name = ?", name).Scan(&data.ID, &data.Name, &data.Password, &data.Status)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

//SelectAllUser 多行查询
func SelectAllUser(datas *users) bool {
	//执行查询语句
	rows, err := db.Query("SELECT * from user")
	if err != nil {
		fmt.Println(err)
		return false
	}
	//循环读取结果
	for rows.Next() {
		var data user
		//将每一行的结果都赋值到一个user对象中
		err := rows.Scan(&data.ID, &data.Name, &data.Password, &data.Status)
		if err != nil {
			fmt.Println(err)
			return false
		}
		//将user追加到users的这个数组中
		*datas = append(*datas, data)
	}
	return true
}

//UpdateUser 更新user
func UpdateUser(data user, tag int) bool {
	if tag == 0 {
		return false
	}
	//开启事务
	tx, err := db.Begin()
	if err != nil {
		fmt.Println(err)
	}
	switch tag {
	case updatePasswordStatus: //更新密码和状态
		//准备sql语句
		stmt, err := tx.Prepare("UPDATE user SET password = ?, status = ? WHERE Name = ?")
		if err != nil {
			fmt.Println(err)
			return false
		}
		//设置参数以及执行sql语句
		_, err = stmt.Exec(data.Password, data.Status, data.Name)
		if err != nil {
			fmt.Println(err)
			return false
		}
	case updatePassword: //只更新密码
		//准备sql语句
		stmt, err := tx.Prepare("UPDATE user SET password = ? WHERE Name = ?")
		if err != nil {
			fmt.Println(err)
			return false
		}
		//设置参数以及执行sql语句
		_, err = stmt.Exec(data.Password, data.Name)
		if err != nil {
			fmt.Println(err)
			return false
		}
	case updateStatus: //只更新状态
		//准备sql语句
		stmt, err := tx.Prepare("UPDATE user SET status = ? WHERE Name = ?")
		if err != nil {
			fmt.Println(err)
			return false
		}
		//设置参数以及执行sql语句
		_, err = stmt.Exec(data.Status, data.Name)
		if err != nil {
			fmt.Println(err)
			return false
		}
	}

	//提交事务
	tx.Commit()
	return true
}

//DeleteUser 删除user
func DeleteUser(data user) bool {
	//开启事务
	tx, err := db.Begin()
	if err != nil {
		fmt.Println(err)
	}
	//准备sql语句
	stmt, err := tx.Prepare("DELETE FROM user WHERE name = ?")
	if err != nil {
		fmt.Println(err)
		return false
	}
	//设置参数以及执行sql语句
	_, err = stmt.Exec(data.Name)
	fmt.Println(err)
	if err != nil {
		fmt.Println(err)
		return false
	}
	//提交事务
	tx.Commit()
	return true
}

//InsertUser 插入user
func InsertUser(data user) bool {
	//开启事务
	tx, err := db.Begin()
	if err != nil {
		fmt.Println(err)
		return false
	}
	//准备sql语句
	stmt, err := tx.Prepare("INSERT INTO user (`name`, `password`, `status`) VALUES (?, ?, ?)")
	if err != nil {
		fmt.Println(err)
		return false
	}
	//将参数传递到sql语句中并且执行
	_, err = stmt.Exec(data.Name, data.Password, data.Status)
	if err != nil {
		fmt.Println(err)
		return false
	}
	//将事务提交
	tx.Commit()

	return true
}
