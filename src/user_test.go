package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestRouter(t *testing.T) {
	router := NewRouter()
	fmt.Println(router)
	router = nil
}

func TestUserPOST(t *testing.T) {
	reqData := &user{
		Name:     "dd",
		Password: "234",
		Status:   true,
	}

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/add", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	AddTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Insert data success!") != true {
		t.Fatal(str)
	}
}

//TestUserGet 获取用户数据
func TestUserGet(t *testing.T) {
	reqData := &user{
		Name: "dd",
	}

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/find", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	FindTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Find user fail!") == true {
		t.Fatal(str)
	}
}

//TestUserGet 获取所有的用户数据
func TestUserGetAll(t *testing.T) {
	reqData := &user{
		Name: "*",
	}

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/find", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	FindTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Find users fail!") == true {
		t.Fatal(str)
	}
}

//TestUserUpdatePasswordStatus 更新密码和状态
type userUpdatePasswordStatus struct {
	Name     string `json:"name"`     //名字
	Password string `json:"password"` //密码
	Status   bool   `json:"status"`   //状态，0:有效帐户 1:无效帐户
}

func TestUserUpdatePasswordStatus(t *testing.T) {
	reqData := &userUpdatePasswordStatus{
		Name:     "dd",
		Password: "234",
		Status:   true,
	}

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("PUT", "/modify", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	ModifyTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Update data success!") != true {
		t.Fatal(str)
	}
}

//TestUserUpdatePassword 更新密码
type userUpdatePassword struct {
	Name     string `json:"name"`     //名字
	Password string `json:"password"` //密码
}

func TestUserUpdatePassword(t *testing.T) {
	reqData := &userUpdatePassword{
		Name:     "dd",
		Password: "234",
	}
	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("PUT", "/modify", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	ModifyTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Update data success!") != true {
		t.Fatal(str)
	}
}

//TestUserUpdateStatus 更新状态
type userUpdateStatus struct {
	Name   string `json:"name"`   //名字
	Status bool   `json:"status"` //状态，0:有效帐户 1:无效帐户
}

func TestUserUpdateStatus(t *testing.T) {
	reqData := &userUpdateStatus{
		Name:   "dd",
		Status: true,
	}
	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("PUT", "/modify", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	ModifyTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Update data success!") != true {
		t.Fatal(str)
	}
}

//TestUserDELETE 删除用户数据
func TestUserDELETE(t *testing.T) {
	reqData := &user{
		Name: "dd",
	}

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("DELETE", "/delete", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	DeleteTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Delete data success!") != true {
		t.Fatal(str)
	}
}

/********************************错误情况检测**************************************/

func TestUserPOSTErrName(t *testing.T) {
	reqData := &user{
		Name:     "dddddddddddddddddddddddddddddddddddddddddddddd",
		Password: "234",
		Status:   true,
	}

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/add", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	AddTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Insert data fail!") != true {
		t.Fatal(str)
	}
}

func TestUserPOSTErrPassword(t *testing.T) {
	reqData := &user{
		Name:     "dd",
		Password: "01234567890123456789012345678901234567890123456789",
		Status:   true,
	}

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/add", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	AddTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Insert data fail!") != true {
		t.Fatal(str)
	}
}

func TestUserGetErr(t *testing.T) {
	reqData := &user{
		Name: "cc",
	}

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/find", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	FindTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Find user fail!") != true {
		t.Fatal(str)
	}
}

//TestUserPOSTErrUser POST错误的数据
func TestUserPOSTErrUser(t *testing.T) {
	reqData := "{\"name\": \"aa\", \"password\":\"123\", \"status\":rue}"

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/add", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	AddTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Insert data success!") == true {
		t.Fatal(str)
	}
}

//TestUserGETErrUser POST错误的数据
func TestUserGETErrUser(t *testing.T) {
	reqData := "{\"nam\": \"aa\"}"

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/find", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	FindTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Find data success!") == true {
		t.Fatal(str)
	}
}

//TestUserPUTErrUser POST错误的数据
func TestUserPUTErrUser(t *testing.T) {
	reqData := "{\"nam\": \"aa\", \"password\":\"123\"}"

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("PUT", "/modify", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	ModifyTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Modify data success!") == true {
		t.Fatal(str)
	}
}

//TestUserDELETEErrUser POST错误的数据
func TestUserDELETEErrUser(t *testing.T) {
	reqData := "{\"nam\": \"aa\"}"

	reqBody, _ := json.Marshal(reqData)
	rw := httptest.NewRecorder()
	req, err := http.NewRequest("DELETE", "/delete", bytes.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}

	DeleteTheUser(rw, req)
	str := string(rw.Body.Bytes())
	if strings.Contains(str, "Delete data success!") == true {
		t.Fatal(str)
	}
}
